# Flectra Community / muk_base

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[muk_fields_lobject](muk_fields_lobject/) | 1.0.2.2.1| PGSQL Large Objects Support
[muk_branding](muk_branding/) | 1.0.1.0.1| Branding and Debranding
[muk_utils](muk_utils/) | 1.0.1.0.16| Utility Features
[muk_converter](muk_converter/) | 1.0.1.2.6| Universal Converter
[muk_session_store](muk_session_store/) | 1.0.1.0.4| Session Store Options
[muk_automation_extension](muk_automation_extension/) | 1.0.1.0.1| Extension for Odoo's Base Automation
[muk_models_groupby_hour](muk_models_groupby_hour/) | 1.0.2.0.1| Group records by hour
[muk_autovacuum](muk_autovacuum/) | 1.0.2.1.4| Configure automatic garbage collection
[muk_thumbnails](muk_thumbnails/) | 1.0.1.0.4| File Thumbnails
[muk_attachment_lobject](muk_attachment_lobject/) | 1.0.2.0.2| Large Objects Attachment Location
[muk_saas_branding](muk_saas_branding/) | 1.0.1.0.0| Branding and Debranding
[muk_security](muk_security/) | 1.0.1.1.8| Security Features


